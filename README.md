# Free Assange Popup

A "Free Assange" popup in Javascript.

Free Assange Popup is licensed under the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE. See section Licence.

## Getting started

Download the file free-assange-popup.js and integrate it to any html document by adding the line

		<script src="path-to-the-file/free-assange-popup.js"></script>

Or, you can use Wikiwix online version of the file with

		<script src="https://archive.wikiwix.com/cache/topleftpopup.js"></script>


## License

        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
                    Version 2, December 2004 

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 

 Everyone is permitted to copy and distribute verbatim or modified 
 copies of this license document, and changing it is allowed as long 
 as the name is changed. 

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 

  0. You just DO WHAT THE FUCK YOU WANT TO.

