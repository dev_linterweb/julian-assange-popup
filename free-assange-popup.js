
addEventListener('load', () => {
    let topleft = document.createElement('img')
    let style_m = 'position:absolute;left:0;top:0;width:15vh;height:15vh;z-index:10000;cursor:pointer;box-shadow: 3px 3px 3px #666;transition: width .5s,height .5s,top .5s,left .5s;'
    let style_b = 'position:absolute;left:2vw;top:2vh;width:45vh;height:45vh;z-index:10000;cursor:pointer;box-shadow: 3px 3px 3px #666;transition: width .5s,height .5s,top .5s,left .5s;'
    let prepareLeaving = false
    let opened = false
    topleft.setAttribute(
        'src',
        'https://archive.wikiwix.com/client/img/m_assange.jpg'
    )
    topleft.setAttribute(
        'style',
        style_m
    )
    let onEnter = ()=>{
        prepareLeaving = false
        setTimeout( ()=>{
                opened = true
            },100
        )
        topleft.setAttribute(
            'src',
            'https://archive.wikiwix.com/client/img/assange.jpg'
        )
        topleft.setAttribute(
            'style',
            style_b
        )
    }
    topleft.addEventListener('mouseenter',onEnter)
    topleft.addEventListener('mouseleave',(e)=>{
        prepareLeaving = true
        setTimeout( ()=>{
                if ( prepareLeaving ) {
                    prepareLeaving = false
                    opened = false
                    topleft.setAttribute(
                        'src',
                        'https://archive.wikiwix.com/client/img/m_assange.jpg'
                    )
                    topleft.setAttribute(
                        'style',
                        style_m
                    )
                }
            },800
        )
    })
    topleft.addEventListener('click',(e)=>{
        if ( opened ) {
            window.open('https://fr.wikipedia.org/wiki/Julian_Assange')
        } else {
            onEnter()
        }
    })

    document.body.insertBefore(
        topleft, 
        document.body.firstChild
    );
});
